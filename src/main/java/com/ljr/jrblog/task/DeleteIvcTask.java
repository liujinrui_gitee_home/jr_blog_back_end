package com.ljr.jrblog.task;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.ljr.jrblog.mapper.JrbIvcMapper;
import com.ljr.jrblog.pojo.JrbIvc;
import lombok.AllArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
@AllArgsConstructor
public class DeleteIvcTask {

    private final JrbIvcMapper jrbIvcMapper;
    @Scheduled(cron = "1 0 0 * * ?")
    public void exec(){
        LambdaQueryWrapper<JrbIvc> wrapper = new LambdaQueryWrapper();
        LocalDateTime localDateTime = LocalDateTime.now().minusDays(1);
        wrapper.le(JrbIvc::getCreateTime, localDateTime);
        jrbIvcMapper.delete(wrapper);
    }
}
