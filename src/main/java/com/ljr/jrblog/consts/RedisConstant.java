package com.ljr.jrblog.consts;

public class RedisConstant {

    private RedisConstant(){}

    //自增key统一前缀
    public static final String ICR_PREFIX = "jrBlog:icr:";
    //分布式锁统一前缀
    public static final String LOCK_PREFIX = "jrBlog:lock:";
    //用户设备时间戳hash大key
    public static final String DEVICE_TIMESTAMP_KEY = "jrBlog-device-timestamp";
    //用户登录
    public static final String LOGIN_PREFIX = "jrBlog:login:";
}
