package com.ljr.jrblog.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ljr.jrblog.common.R;
import com.ljr.jrblog.pojo.JrbArticle;
import com.ljr.jrblog.pojo.JrbUser;

public interface JrbArticleService extends IService<JrbArticle> {
}
