package com.ljr.jrblog.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ljr.jrblog.common.R;
import com.ljr.jrblog.pojo.JrbUser;
import com.ljr.jrblog.pojo.JrbUserInfo;

public interface JrbUserInfoService extends IService<JrbUserInfo> {
    R updateByToken(JrbUserInfo jrbUserInfo);
}
