package com.ljr.jrblog.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ljr.jrblog.common.R;
import com.ljr.jrblog.pojo.JrbUser;
import com.ljr.jrblog.pojo.UpdatePasswordDto;

import javax.servlet.http.HttpServletRequest;

public interface JrbUserService extends IService<JrbUser> {
    R register(JrbUser jrbUser) throws Exception;

    R login(HttpServletRequest request, JrbUser jrbUser) throws InterruptedException;

    R updatePassword(UpdatePasswordDto dto, Long userId);
}
