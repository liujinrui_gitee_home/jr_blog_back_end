package com.ljr.jrblog.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ljr.jrblog.pojo.JrbArticle;
import com.ljr.jrblog.pojo.JrbArticleData;

public interface JrbArticleDataService extends IService<JrbArticleData> {
}
