package com.ljr.jrblog.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ljr.jrblog.common.R;
import com.ljr.jrblog.pojo.JrbStuCourseRecord;
import com.ljr.jrblog.pojo.JrbUser;

import java.util.List;

public interface JrbStuCourseRecordService extends IService<JrbStuCourseRecord> {
    R getAllNames(Long userId);

    R deleteBatchIds(Long userId, Long[] ids);
}
