package com.ljr.jrblog.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ljr.jrblog.common.R;
import com.ljr.jrblog.mapper.JrbArticleMapper;
import com.ljr.jrblog.mapper.JrbUserInfoMapper;
import com.ljr.jrblog.mapper.JrbUserMapper;
import com.ljr.jrblog.pojo.JrbArticle;
import com.ljr.jrblog.pojo.JrbUser;
import com.ljr.jrblog.pojo.JrbUserInfo;
import com.ljr.jrblog.service.JrbArticleService;
import com.ljr.jrblog.service.JrbUserService;
import com.ljr.jrblog.util.BizUtil;
import com.ljr.jrblog.util.TokenUtils;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;

import java.nio.charset.StandardCharsets;
import java.util.UUID;

@Service
@AllArgsConstructor
public class JrbArticleServiceImpl extends ServiceImpl<JrbArticleMapper, JrbArticle> implements JrbArticleService {


}
