package com.ljr.jrblog.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ljr.jrblog.common.R;
import com.ljr.jrblog.context.BaseContext;
import com.ljr.jrblog.mapper.JrbCategoryMapper;
import com.ljr.jrblog.mapper.JrbUserInfoMapper;
import com.ljr.jrblog.pojo.JrbCategory;
import com.ljr.jrblog.pojo.JrbUserInfo;
import com.ljr.jrblog.service.JrbCategoryService;
import com.ljr.jrblog.service.JrbUserInfoService;
import com.ljr.jrblog.util.BizUtil;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Service
@AllArgsConstructor
public class JrbCategoryServiceImpl extends ServiceImpl<JrbCategoryMapper, JrbCategory> implements JrbCategoryService {

    @Override
    @Transactional
    public R add(JrbCategory category) throws Exception {
        Long count = lambdaQuery().eq(JrbCategory::getName, category.getName())
                .eq(JrbCategory::getDelFlag, 0).count();
        if(count > 0) return R.fail("分类已存在");
        category.setUserId(BaseContext.getUserId());
        BizUtil.setInsertProperties(category);
        baseMapper.insert(category);
        return R.ok();
    }

    @Override
    public R bizUpdate(JrbCategory category) {
        JrbCategory one = baseMapper.selectById(category.getId());
        Long userId = BaseContext.getUserId();
        if(!one.getUserId().equals(userId)) return R.fail("无修改权限");
        Long count = lambdaQuery().eq(JrbCategory::getName, category.getName())
                .eq(JrbCategory::getDelFlag, 0).count();
        if(count > 0) return R.fail("分类已存在");
        one.setName(category.getName());
        one.setUpdateTime(LocalDateTime.now());
        baseMapper.updateById(one);
        return R.ok();
    }
}
