package com.ljr.jrblog.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ljr.jrblog.common.R;
import com.ljr.jrblog.consts.RedisConstant;
import com.ljr.jrblog.mapper.JrbIvcMapper;
import com.ljr.jrblog.mapper.JrbUserInfoMapper;
import com.ljr.jrblog.mapper.JrbUserMapper;
import com.ljr.jrblog.pojo.JrbIvc;
import com.ljr.jrblog.pojo.JrbUser;
import com.ljr.jrblog.pojo.JrbUserInfo;
import com.ljr.jrblog.pojo.UpdatePasswordDto;
import com.ljr.jrblog.service.JrbIvcService;
import com.ljr.jrblog.service.JrbUserService;
import com.ljr.jrblog.util.BizUtil;
import com.ljr.jrblog.util.TokenUtils;
import lombok.AllArgsConstructor;
import org.apache.ibatis.annotations.Param;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;

import javax.servlet.http.HttpServletRequest;
import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Service
@AllArgsConstructor
public class JrbIvcServiceImpl extends ServiceImpl<JrbIvcMapper, JrbIvc> implements JrbIvcService {
}
