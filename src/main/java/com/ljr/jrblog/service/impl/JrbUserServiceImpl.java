package com.ljr.jrblog.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ljr.jrblog.common.R;
import com.ljr.jrblog.consts.RedisConstant;
import com.ljr.jrblog.mapper.JrbIvcMapper;
import com.ljr.jrblog.mapper.JrbUserInfoMapper;
import com.ljr.jrblog.mapper.JrbUserMapper;
import com.ljr.jrblog.pojo.JrbIvc;
import com.ljr.jrblog.pojo.JrbUser;
import com.ljr.jrblog.pojo.JrbUserInfo;
import com.ljr.jrblog.pojo.UpdatePasswordDto;
import com.ljr.jrblog.service.JrbIvcService;
import com.ljr.jrblog.service.JrbUserService;
import com.ljr.jrblog.util.BizUtil;
import com.ljr.jrblog.util.TokenUtils;
import lombok.AllArgsConstructor;
import org.redisson.Redisson;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;
import org.springframework.web.context.request.RequestContextHolder;

import javax.servlet.http.HttpServletRequest;
import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Service
@AllArgsConstructor
public class JrbUserServiceImpl extends ServiceImpl<JrbUserMapper, JrbUser> implements JrbUserService{

    private final JrbUserMapper userMapper;
    private final JrbUserInfoMapper userInfoMapper;
    private final RedissonClient redissonClient;
    private final StringRedisTemplate stringRedisTemplate;
    private final JrbIvcService ivcService;
    @Override
    @Transactional
    public R register(JrbUser jrbUser) throws Exception {
        Long ivcId = jrbUser.getIvcId();
        String ivcCode = jrbUser.getIvcCode();
        JrbIvc ivc = ivcService.getById(ivcId);
        if(ivc == null) return R.fail("图形码已过期!");
        if(!ivc.getContent().equals(ivcCode)) return R.fail("图形码错误!");
        Long count = lambdaQuery().eq(JrbUser::getUsername, jrbUser.getUsername())
                .eq(JrbUser::getDelFlag, 0).count();
        if(count > 0) return R.fail("用户名已存在");
        String salt = BizUtil.get10RandomSalt();
        String password = DigestUtils.md5DigestAsHex((jrbUser.getPassword() + salt).getBytes(StandardCharsets.UTF_8));
        jrbUser.setSalt(salt);
        jrbUser.setPassword(password);
        BizUtil.setInsertProperties(jrbUser);
        userMapper.insert(jrbUser);
        //同步新增一条userInfo
        JrbUserInfo userInfo = new JrbUserInfo();
        userInfo.setNickName(UUID.randomUUID().toString().substring(0,10));
        userInfo.setUserId(jrbUser.getId());
        userInfo.setMotto("自由像风~");
        BizUtil.setInsertProperties(userInfo);
        userInfoMapper.insert(userInfo);
        return R.ok();
    }

    @Override
    @Transactional
    public R login(HttpServletRequest request, JrbUser jrbUser) throws InterruptedException {
        RLock lock = redissonClient.getLock(RedisConstant.LOCK_PREFIX + "login:" + jrbUser.getUsername());
        boolean flag = lock.tryLock(1, 10, TimeUnit.SECONDS);
        if(flag){
            try {
                String ipAddress = request.getHeader("X-Forwarded-For");
                if (ipAddress == null) {
                    ipAddress = request.getRemoteAddr();
                }
                // 如果X-Forwarded-For存在多个值（可能是经过多个代理），通常第一个值是最原始的IP
                if (ipAddress != null && ipAddress.contains(",")) {
                    ipAddress = ipAddress.split(",")[0].trim();
                }
                Set<String> keys = stringRedisTemplate.keys(RedisConstant.LOGIN_PREFIX +
                        jrbUser.getUsername()+":"+ipAddress + ":*");
                if(keys == null) keys = new HashSet<>();
                if(keys.size() >= 10) return R.fail("登录频繁!");
                String random = BizUtil.get10RandomSalt();
                stringRedisTemplate.opsForValue().set(RedisConstant.LOGIN_PREFIX+jrbUser.getUsername()
                        +":"+ipAddress + ":" +random,"1",300,TimeUnit.SECONDS);
                Long ivcId = jrbUser.getIvcId();
                String ivcCode = jrbUser.getIvcCode();
                JrbIvc ivc = ivcService.getById(ivcId);
                if(ivc == null) return R.fail("图形码已过期!");
                if(!ivc.getContent().equals(ivcCode)) return R.fail("图形码错误!");
                JrbUser one = lambdaQuery().eq(JrbUser::getUsername, jrbUser.getUsername())
                        .eq(JrbUser::getDelFlag, 0).last("limit 1").one();
                if(one == null) return R.fail("当前用户名不存在");
                String password = DigestUtils.md5DigestAsHex((jrbUser.getPassword() + one.getSalt()).getBytes(StandardCharsets.UTF_8));
                if(!password.equals(one.getPassword())) return R.fail("密码错误");
                String token = TokenUtils.getToken(one.getId(),jrbUser.getTimeStamp());
                //刷新redis里面的用户时间戳(代表了最新的设备信息)数据
                stringRedisTemplate.opsForHash().put(RedisConstant.DEVICE_TIMESTAMP_KEY,
                        one.getId().toString(),
                        jrbUser.getTimeStamp().toString());
                return R.ok(token);
            }finally {
                lock.unlock();
            }
        }else {
            return R.fail("请求频繁,稍后重试!");
        }
    }

    @Override
    @Transactional
    public R updatePassword(UpdatePasswordDto dto, Long userId) {
        JrbUser byId = getById(userId);
        if(byId == null || byId.getDelFlag().equals(1)) return R.fail("当前用户不存在或已注销");
        String password = DigestUtils.md5DigestAsHex((dto.getOldPassword() + byId.getSalt()).getBytes(StandardCharsets.UTF_8));
        if(!password.equals(byId.getPassword())) return R.fail("密码错误");
        String finalPassword = DigestUtils.md5DigestAsHex((dto.getNewPassword() + byId.getSalt()).getBytes(StandardCharsets.UTF_8));
        baseMapper.updatePasswordById(userId,finalPassword);
        return R.ok();
    }

}
