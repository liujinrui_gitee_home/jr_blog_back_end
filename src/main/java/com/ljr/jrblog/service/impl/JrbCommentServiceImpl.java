package com.ljr.jrblog.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ljr.jrblog.mapper.JrbCommentMapper;
import com.ljr.jrblog.mapper.JrbLikeMapper;
import com.ljr.jrblog.pojo.JrbComment;
import com.ljr.jrblog.pojo.JrbLike;
import com.ljr.jrblog.service.JrbCommentService;
import com.ljr.jrblog.service.JrbLikeService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class JrbCommentServiceImpl extends ServiceImpl<JrbCommentMapper, JrbComment>
        implements JrbCommentService {


}
