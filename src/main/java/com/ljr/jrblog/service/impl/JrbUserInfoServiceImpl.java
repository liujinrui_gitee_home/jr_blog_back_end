package com.ljr.jrblog.service.impl;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ljr.jrblog.common.R;
import com.ljr.jrblog.context.BaseContext;
import com.ljr.jrblog.mapper.JrbUserInfoMapper;
import com.ljr.jrblog.pojo.JrbUserInfo;
import com.ljr.jrblog.service.JrbUserInfoService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.time.LocalDateTime;

@Service
@AllArgsConstructor
public class JrbUserInfoServiceImpl extends ServiceImpl<JrbUserInfoMapper, JrbUserInfo> implements JrbUserInfoService {

    @Override
    @Transactional
    public R updateByToken(JrbUserInfo jrbUserInfo) {
        Long userId = BaseContext.getUserId();
        JrbUserInfo one = lambdaQuery().eq(JrbUserInfo::getUserId, userId).one();
        jrbUserInfo.setId(one.getId());
        jrbUserInfo.setDelFlag(null);
        jrbUserInfo.setCreateTime(null);
        jrbUserInfo.setUpdateTime(LocalDateTime.now());
        baseMapper.updateById(jrbUserInfo);
        return R.ok();
    }
}
