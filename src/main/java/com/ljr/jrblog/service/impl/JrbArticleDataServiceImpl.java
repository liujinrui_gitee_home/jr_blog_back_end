package com.ljr.jrblog.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ljr.jrblog.mapper.JrbArticleDataMapper;
import com.ljr.jrblog.mapper.JrbArticleMapper;
import com.ljr.jrblog.pojo.JrbArticle;
import com.ljr.jrblog.pojo.JrbArticleData;
import com.ljr.jrblog.service.JrbArticleDataService;
import com.ljr.jrblog.service.JrbArticleService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class JrbArticleDataServiceImpl extends ServiceImpl<JrbArticleDataMapper, JrbArticleData>
        implements JrbArticleDataService {


}
