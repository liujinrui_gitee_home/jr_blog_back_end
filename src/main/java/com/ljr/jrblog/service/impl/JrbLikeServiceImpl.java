package com.ljr.jrblog.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ljr.jrblog.mapper.JrbArticleDataMapper;
import com.ljr.jrblog.mapper.JrbLikeMapper;
import com.ljr.jrblog.pojo.JrbArticleData;
import com.ljr.jrblog.pojo.JrbLike;
import com.ljr.jrblog.service.JrbArticleDataService;
import com.ljr.jrblog.service.JrbLikeService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class JrbLikeServiceImpl extends ServiceImpl<JrbLikeMapper, JrbLike>
        implements JrbLikeService {


}
