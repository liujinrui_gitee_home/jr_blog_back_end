package com.ljr.jrblog.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ljr.jrblog.common.R;
import com.ljr.jrblog.context.BaseContext;
import com.ljr.jrblog.mapper.JrbStuCourseRecordMapper;
import com.ljr.jrblog.mapper.JrbUserInfoMapper;
import com.ljr.jrblog.pojo.JrbStuCourseRecord;
import com.ljr.jrblog.pojo.JrbUserInfo;
import com.ljr.jrblog.service.JrbStuCourseRecordService;
import com.ljr.jrblog.service.JrbUserInfoService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
@AllArgsConstructor
public class JrbStuCourseRecordServiceImpl extends ServiceImpl<JrbStuCourseRecordMapper, JrbStuCourseRecord> implements JrbStuCourseRecordService {

    @Override
    public R getAllNames(Long userId) {
        List<String> res = baseMapper.getAllNames(userId);
        if(res == null) res = new ArrayList<>();
        return R.ok(res);
    }

    @Override
    public R deleteBatchIds(Long userId, Long[] ids) {
        LambdaQueryWrapper<JrbStuCourseRecord> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(JrbStuCourseRecord::getUserId, userId)
                .in(JrbStuCourseRecord::getId, ids);
        baseMapper.delete(wrapper);
        return R.ok();
    }
}
