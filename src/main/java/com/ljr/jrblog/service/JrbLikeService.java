package com.ljr.jrblog.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ljr.jrblog.pojo.JrbArticleData;
import com.ljr.jrblog.pojo.JrbLike;

public interface JrbLikeService extends IService<JrbLike> {
}
