package com.ljr.jrblog.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ljr.jrblog.common.R;
import com.ljr.jrblog.pojo.JrbCategory;
import com.ljr.jrblog.pojo.JrbUser;

public interface JrbCategoryService extends IService<JrbCategory> {
    R add(JrbCategory category) throws Exception;

    R bizUpdate(JrbCategory category);
}
