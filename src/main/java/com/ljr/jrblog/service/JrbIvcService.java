package com.ljr.jrblog.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ljr.jrblog.common.R;
import com.ljr.jrblog.pojo.JrbIvc;
import com.ljr.jrblog.pojo.JrbUser;
import com.ljr.jrblog.pojo.UpdatePasswordDto;

import javax.servlet.http.HttpServletRequest;

public interface JrbIvcService extends IService<JrbIvc> {
}
