package com.ljr.jrblog.service;

import com.ljr.jrblog.common.R;
import com.ljr.jrblog.config.MinioConfig;
import io.minio.*;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import java.io.InputStream;
import java.time.LocalDate;
import java.util.UUID;

@Component
@AllArgsConstructor
@Slf4j
public class MinioFileService {

    private final MinioConfig minioConfig;

    @Qualifier("myMinioClient")
    private final MinioClient minioClient;

    public R fileDelete(String filePath){
        filePath = filePath.substring(filePath.indexOf(minioConfig.getBucket())
                + minioConfig.getBucket().length() + 1, filePath.length());
        // 删除Objects
        RemoveObjectArgs removeObjectArgs = RemoveObjectArgs.builder()
                .bucket(minioConfig.getBucket())
                .object(filePath).build();
        try{
            minioClient.removeObject(removeObjectArgs);
        }catch (Exception e){
            System.out.println(e);
            return R.fail("内部错误");
        }
        return R.ok();
    }

    public String fileUpload(MultipartFile file) {
        String format = "";
        String[] split = file.getOriginalFilename().split("\\.");
        if(split != null && split.length > 0){
            format = split[split.length - 1];
        }
        try {
            //2.封装信息
            //获取一个目标文件的输入流
            LocalDate now = LocalDate.now();
            int year = now.getYear();
            int monthValue = now.getMonthValue();
            int dayOfMonth = now.getDayOfMonth();
            InputStream fileInputStream = file.getInputStream();
            String objectName = year+"/"+monthValue+"/"+dayOfMonth+"/"+UUID.randomUUID()+"."+format;
            PutObjectArgs putObjectArgs = PutObjectArgs.builder()
                    .object(objectName)//文件名,如果其中带"/"会被解析成文件夹
                    //.contentType(contentType)//文件类型
                    .bucket(minioConfig.getBucket())//桶名,需要和客户端一致
                    //读取文件所有内容的固定写法
                    .stream(fileInputStream,fileInputStream.available(),-1).build();
            //3.执行上传
            minioClient.putObject(putObjectArgs);
            //4.执行完成之后这个连接打开就是静态资源展示
            return minioClient.getObjectUrl(minioConfig.getBucket(),objectName);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
