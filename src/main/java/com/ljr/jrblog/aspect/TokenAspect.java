package com.ljr.jrblog.aspect;

import com.ljr.jrblog.common.R;
import com.ljr.jrblog.consts.RedisConstant;
import com.ljr.jrblog.context.BaseContext;
import com.ljr.jrblog.util.BizUtil;
import com.ljr.jrblog.util.RequestContextUtil;
import com.ljr.jrblog.util.TokenUtils;
import lombok.AllArgsConstructor;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import java.util.Map;

@Aspect
@Component
@AllArgsConstructor
public class TokenAspect {
    @Pointcut("execution(* com.ljr.jrblog.controller.*.*(..)) && @annotation(com.ljr.jrblog.annos.TokenRequired)")
    public void definePt(){}

    private final StringRedisTemplate redisTemplate;
    @Around("definePt()")
    public Object around(ProceedingJoinPoint pjp) throws Throwable {
        String token = RequestContextUtil.getHeader("User-Token");
        if(!BizUtil.isNotBlankStr(token)){
            return R.noAuth();
        }
        Long userId;
        try {
            Map<String, Long> map = TokenUtils.parseToken(token);
            userId = map.get("userId");
            Long tokenTimeStamp = map.get("timeStamp");
            if(userId == null || tokenTimeStamp == null) return R.noAuth();
            Long redisTimeStamp = Long.parseLong((String) redisTemplate.opsForHash()
                    .get(RedisConstant.DEVICE_TIMESTAMP_KEY, userId.toString()));
            if(!redisTimeStamp.equals(tokenTimeStamp)) return R.noAuth();
        }catch (Exception e){
            return R.noAuth();
        }
        BaseContext.setUserId(userId);
        Object proceed = pjp.proceed();
        BaseContext.remove();
        return proceed;
    }
}
