package com.ljr.jrblog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.scheduling.annotation.EnableScheduling;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableAspectJAutoProxy
@EnableScheduling
public class JrBlogApplication {
    public static void main(String[] args) {
        SpringApplication.run(JrBlogApplication.class, args);
    }
}
