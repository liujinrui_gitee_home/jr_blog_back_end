//package com.ljr.jrblog.config;
//
//import com.ljr.jrblog.interceptor.LoginTokenInterceptor;
//import lombok.AllArgsConstructor;
//import org.springframework.stereotype.Component;
//import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
//import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
//
//@Component
//@AllArgsConstructor
//public class MvcConfig implements WebMvcConfigurer {
//
//    private final LoginTokenInterceptor loginTokenInterceptor;
//    @Override
//    public void addInterceptors(InterceptorRegistry registry) {
//        registry.addInterceptor(loginTokenInterceptor)
//                .excludePathPatterns("/user/login")
//                .excludePathPatterns("/user/register");
//    }
//}
