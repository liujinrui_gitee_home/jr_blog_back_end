package com.ljr.jrblog.config;


import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.client.codec.StringCodec;
import org.redisson.config.Config;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class RedissonConfig {

	private final int database=0;

	@Value("${spring.redis.host}")
	private String host;

	@Value("${spring.redis.port}")
	private String port;

	@Value("${spring.redis.password}")
	private String password;

	@Bean(value = "redissonClient", destroyMethod = "shutdown")
	public RedissonClient redissonClient() throws Exception{

		Config config = new Config();
		config.useSingleServer().setAddress(String.format("redis://%s:%s", this.host, this.port));
		config.useSingleServer().setPassword(this.password);
		config.useSingleServer().setDatabase(this.database);
		config.useSingleServer().setRetryAttempts(3);  // 重试3次
		config.useSingleServer().setTimeout(5000);     // 超时时间5秒

		StringCodec codec = new StringCodec();
		config.setCodec(codec);
		return Redisson.create(config);
	}

}
