package com.ljr.jrblog.config;

import io.minio.MinioClient;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.unit.DataSize;

import javax.servlet.MultipartConfigElement;

@ConfigurationProperties(prefix = "minio")
@Configuration
@Data
public class MinioConfig {
    private String username;
    private String password;
    private String bucket;
    private String endpoint;
    @Bean(name = "myMinioClient")
    public MinioClient minioClient() {
        //1.获取minio客户端连接
        return MinioClient.builder()
                .credentials(username,password)//账号密码
                .endpoint(endpoint)//minio的服务地址
                .build();
    }
    @Bean
    public MultipartConfigElement multipartConfigElement() {
        MultipartConfigFactory factory = new MultipartConfigFactory();
        //允许上传的文件最大值
        factory.setMaxFileSize(DataSize.parse("50MB")); //KB,MB
        /// 设置总上传数据总大小
        factory.setMaxRequestSize(DataSize.parse("50MB"));
        return factory.createMultipartConfig();
    }
}
