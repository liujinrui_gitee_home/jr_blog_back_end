package com.ljr.jrblog.context;

public class BaseContext {
    //存放用户id
    private static final ThreadLocal<Long> threadLocal =new ThreadLocal<>();

    public static Long getUserId() {
        return threadLocal.get();
    }

    public static void setUserId(Long userId) {
        threadLocal.set(userId);
    }
    public static void remove() {
        threadLocal.remove();
    }
}
