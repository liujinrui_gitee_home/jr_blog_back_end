package com.ljr.jrblog.util;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import javax.net.ssl.*;
import java.io.*;
import java.lang.reflect.Field;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.X509Certificate;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 业务工具类,日常开发中需要重复使用的一些的方法抽取放在这个工具类里面
 * 每个方法都配备了详细的注解来便于理解和使用
 */
public class BizUtil {

    private BizUtil(){}

    public static void pageParamValid(Page page){
        if(page == null) page = new Page();
        if(page.getCurrent() < 1) page.setCurrent(1);
        if(page.getSize() < 1) page.setSize(10);
        if(page.getSize() > 1000) page.setSize(1000);
    }

    /**
     * 获取长度为10位的随机字符串
     * @return
     */
    public static String get10RandomSalt(){
        return UUID.randomUUID().toString().replace("-","").substring(0, 10);
    }

    public static List<String> inertMatch(String target,String prefix,String suffix){
        List<String> res = new ArrayList<>();
        Pattern pattern = Pattern.compile(prefix+"(.*?)"+suffix);
        Matcher matcher = pattern.matcher(target);
        while (matcher.find()) {
            res.add(matcher.group(1)); // group(1) 捕获的是第一个括号内的内容
        }
        return res;
    }

    public static byte[] inputStreamToByteArray(InputStream inputStream) throws IOException {
        if(inputStream == null) throw new RuntimeException();
        final int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        int len = 0;
        while (-1 != (len = inputStream.read(buffer))) {
            bos.write(buffer, 0, len);
        }
        return bos.toByteArray();
    }
    public static List<List<String>> cartesianProduct(Collection<String>... strCollectionList){
        List<List<String>> res = new ArrayList<>();
        if(!isNotEmptyCollection(strCollectionList)) return res;
        for (Collection<String> strings : strCollectionList) {
            if(strings == null) throw new IllegalArgumentException();
            removeNullAndEmptyStr(strings);
            if(strings.isEmpty()) return res;
        }
        cartesianProductHelper(strCollectionList, 0, new ArrayList<>(), res);
        return res;
    }

    private static void cartesianProductHelper(Collection<String>[] lists, int index, List<String> currentTuple, List<List<String>> result) {
        if (index == lists.length) {
            // 当遍历完所有列表时，将当前组合添加到结果中
            result.add(new ArrayList<>(currentTuple));
            return;
        }
        // 对当前列表的每个元素进行遍历
        for (String item : lists[index]) {
            // 将当前元素添加到当前组合中
            currentTuple.add(item);
            // 递归处理下一个列表
            cartesianProductHelper(lists, index + 1, currentTuple, result);
            // 回溯，移除当前元素以便尝试下一个元素
            currentTuple.remove(currentTuple.size() - 1);
        }
    }

    /**
     * 移除字符串列表中的null和""
     * @param strCollectionList
     */
    public static void removeNullAndEmptyStr(Collection<String>... strCollectionList){
        for (Collection<String> strings : strCollectionList) {
            strings.removeIf(next -> !isNotBlankStr(next));
        }
    }

    /**
     * 发起http请求并获取结果
     *
     * @param requestUrl 请求地址
     * @param requestMethod 请求方式（GET、post）
     * @param outputStr 提交的数据
     * @return json字符串
     */
    public static String httpRequest(String requestUrl, String requestMethod, Map<String,String> requestHeaders, String outputStr) throws IOException {
        StringBuilder stringBuilder = new StringBuilder();
        URL url = new URL(requestUrl);
        HttpURLConnection httpUrlConn = (HttpURLConnection) url.openConnection();
        httpUrlConn.setRequestMethod(requestMethod);
        if(!isNotBlankStr(outputStr)){
            httpUrlConn.setDoOutput(false);
        }else {
            httpUrlConn.setDoOutput(true);
        }
        httpUrlConn.setDoInput(true);
        httpUrlConn.setUseCaches(false);
        if(requestHeaders != null && !requestHeaders.isEmpty()){
            for (Map.Entry<String, String> entry : requestHeaders.entrySet()) {
                httpUrlConn.addRequestProperty(entry.getKey(),entry.getValue());
            }
        }
        // 当有数据需要提交时
        if (null != outputStr) {
            OutputStream outputStream = httpUrlConn.getOutputStream();
            // 注意编码格式，防止中文乱码
            outputStream.write(outputStr.getBytes("UTF-8"));
            outputStream.close();
        }

        // 将返回的输入流转换成字符串
        InputStream inputStream = httpUrlConn.getInputStream();
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream, "utf-8");
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

        String str = null;
        while ((str = bufferedReader.readLine()) != null) {
            stringBuilder.append(str);
        }
        bufferedReader.close();
        inputStreamReader.close();
        // 释放资源
        inputStream.close();
        httpUrlConn.disconnect();
        return stringBuilder.toString();
    }
    public static String httpRequest(String requestUrl, String requestMethod) throws IOException {
        return httpRequest(requestUrl,requestMethod,null,null);
    }
    public static String httpRequest(String requestUrl, String requestMethod, Map<String,String> requestHeaders) throws IOException {
        return httpRequest(requestUrl,requestMethod,requestHeaders,null);
    }
    public static String httpRequest(String requestUrl, String requestMethod, String outputStr) throws IOException {
        return httpRequest(requestUrl,requestMethod,null,outputStr);
    }
    public static String httpsRequest(String requestUrl, String requestMethod, Map<String,String> requestHeaders, String outputStr) throws IOException, NoSuchAlgorithmException, NoSuchProviderException, KeyManagementException {
        StringBuilder stringBuilder = new StringBuilder();
        // 创建SSLContext对象，并使用我们制定的信任管理器初始化
        TrustManager[] tm = { new BizUtil.MyX509TrustManager() };
        SSLContext sslContext = SSLContext.getInstance("SSL", "SunJSSE");
        sslContext.init(null, tm, new java.security.SecureRandom());
        // 从上述SSLContext对象中得到SSLSocketFactory对象
        SSLSocketFactory ssf = sslContext.getSocketFactory();

        URL url = new URL(requestUrl);
        HttpsURLConnection httpsUrlConn = (HttpsURLConnection) url.openConnection();
        httpsUrlConn.setSSLSocketFactory(ssf);
        httpsUrlConn.setRequestMethod(requestMethod);
        if(!isNotBlankStr(outputStr)){
            httpsUrlConn.setDoOutput(false);
        }else {
            httpsUrlConn.setDoOutput(true);
        }
        httpsUrlConn.setDoInput(true);
        httpsUrlConn.setUseCaches(false);
        if(requestHeaders != null && !requestHeaders.isEmpty()){
            for (Map.Entry<String, String> entry : requestHeaders.entrySet()) {
                httpsUrlConn.addRequestProperty(entry.getKey(),entry.getValue());
            }
        }
        // 当有数据需要提交时
        if (null != outputStr) {
            OutputStream outputStream = httpsUrlConn.getOutputStream();
            // 注意编码格式，防止中文乱码
            outputStream.write(outputStr.getBytes("UTF-8"));
            outputStream.close();
        }

        // 将返回的输入流转换成字符串
        InputStream inputStream = httpsUrlConn.getInputStream();
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream, "utf-8");
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

        String str = null;
        while ((str = bufferedReader.readLine()) != null) {
            stringBuilder.append(str);
        }
        bufferedReader.close();
        inputStreamReader.close();
        // 释放资源
        inputStream.close();
        httpsUrlConn.disconnect();
        return stringBuilder.toString();
    }
    /**
     * 证书信任管理器（用于https请求）
     */
    private static class MyX509TrustManager implements X509TrustManager {

        @Override
        public void checkClientTrusted(X509Certificate[] x509Certificates, String s) {

        }

        @Override
        public void checkServerTrusted(X509Certificate[] x509Certificates, String s) {

        }

        @Override
        public X509Certificate[] getAcceptedIssuers() {
            return new X509Certificate[0];
        }
    }
    public static String httpsRequest(String requestUrl, String requestMethod) throws IOException, NoSuchAlgorithmException, NoSuchProviderException, KeyManagementException {
        return httpsRequest(requestUrl,requestMethod,null,null);
    }
    public static String httpsRequest(String requestUrl, String requestMethod, Map<String,String> requestHeaders) throws IOException, NoSuchAlgorithmException, NoSuchProviderException, KeyManagementException {
        return httpsRequest(requestUrl,requestMethod,requestHeaders,null);
    }
    public static String httpsRequest(String requestUrl, String requestMethod, String outputStr) throws IOException, NoSuchAlgorithmException, NoSuchProviderException, KeyManagementException {
        return httpsRequest(requestUrl,requestMethod,null,outputStr);
    }



    public static void copyProperties(Object from,Object to,boolean copyNull) throws IllegalAccessException {
        if(from == null || to == null) throw new IllegalArgumentException();
        Class<?> aClassFrom = from.getClass();
        Class<?> aClassTo = to.getClass();
        Field[] declaredFieldsFrom = aClassFrom.getDeclaredFields();
        Field[] declaredFieldsTo = aClassTo.getDeclaredFields();
        for (int i = 0; i < declaredFieldsFrom.length; i++) {
            for (int j = 0; j < declaredFieldsTo.length; j++) {
                Field fieldFrom = declaredFieldsFrom[i];
                Field fieldTo = declaredFieldsTo[j];
                if(fieldFrom.getClass().equals(fieldTo.getClass()) && fieldFrom.getName().equals(fieldTo.getName())){
                    fieldFrom.setAccessible(true);
                    fieldTo.setAccessible(true);
                    Object o = fieldFrom.get(from);
                    if(o == null){
                        if(copyNull){
                            fieldTo.set(to,null);
                        }
                    }else {
                        fieldTo.set(to,o);
                    }
                }
            }
        }
    }

    public static String FileToStringByBase64(File file) throws IOException {
        if(file == null) return "";
        Path path = file.toPath();
        byte[] fileBytes = Files.readAllBytes(path);
        byte[] encodedBytes = Base64.getEncoder().encode(fileBytes);
        return new String(encodedBytes);
    }


    public static final String CORRECT = "√";
    public static final String ERROR = "×";
    public static final DateTimeFormatter DATE_TIME_FORMATTER1 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    public static final DateTimeFormatter DATE_TIME_FORMATTER2 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    /**
     * double转int类型,根据第一位小数进行四舍五入
     * @param d
     * @return
     */
    public static int doubleToInteger(double d){
        String string = Double.toString(d);
        if(!string.contains(".")) return Integer.parseInt(string);
        String[] split = string.split("\\.");
        if(Integer.parseInt(Character.toString(split[1].charAt(0))) < 5) return Integer.parseInt(split[0]);
        return Integer.parseInt(split[0]) + 1;
    }

    //赤道半径(米)
    private static final double EQUATOR_RADIUS = 6378137;

    /**
     * 计算两个经纬度点之间的距离
     */
    public static double getDistance(double longitude1, double latitude1, double longitude2, double latitude2) {
        // 纬度
        double lat1 = Math.toRadians(latitude1);
        double lat2 = Math.toRadians(latitude2);
        // 经度
        double lon1 = Math.toRadians(longitude1);
        double lon2 = Math.toRadians(longitude2);
        // 纬度之差
        double a = lat1 - lat2;
        // 经度之差
        double b = lon1 - lon2;
        // 计算两点距离的公式
        double s = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a / 2), 2) + Math.cos(lat1) * Math.cos(lat2) * Math.pow(Math.sin(b / 2), 2)));
        // 弧长乘赤道半径, 返回单位: 米
        s = s * EQUATOR_RADIUS;
        return s;
    }

    /**
     * 不是null和空集合
     * true表示都不是,false表示存在null
     */
    public static boolean isNotEmptyCollection(Collection ... collections){
        boolean res = true;
        for (Collection collection : collections) {
            if(collection == null || collection.isEmpty()){
                res = false;
                break;
            }
        }
        return res;
    }

    /**
     * double加法,不会四舍五入,不建议在需要精确计算的地方使用
     * 只保留两位小数
     * @param d1
     * @param d2
     * @return
     */
    public static double doublePlus(Double d1,Double d2){
        if(d1 == null) d1 = 0.0;
        if(d2 == null) d2 = 0.0;
        Double res = d1 + d2;
        String resStr = res.toString();
        if(resStr.contains(".")){
            String[] split = resStr.split("\\.");
            if(split[1].length() > 2){
                resStr = split[0] + "." + split[1].substring(0,2);
            }
        }
        return Double.parseDouble(resStr);
    }

    /**
     * 判断对象都不是null
     * true表示都不是,false表示存在null
     */
    public static boolean isNotNull(Object ... objects){
        boolean res = true;
        for (Object o : objects) {
            if(o == null){
                res = false;
                break;
            }
        }
        return res;
    }

    /**
     * 字符串不是null和""
     * @param stringList
     * true表示都不是,false表示存在
     */
    public static boolean isNotBlankStr(String ... stringList){
        boolean res = true;
        for (String s : stringList) {
            if(s == null || s.isEmpty()){
                res = false;
                break;
            }
        }
        return res;
    }
    /**
     * 复制第一个对象的channelAccountId和tenantId给第二个对象
     * @return
     */
    public static void copyChannelAccountIdAndTenantId(Object object1,Object object2) throws Exception {
        Class<?> aClass1 = object1.getClass();
        Field channelAccountIdField = aClass1.getDeclaredField("channelAccountId");
        channelAccountIdField.setAccessible(true);
        Object channelAccountId = channelAccountIdField.get(object1);
        Field tenantIdField = aClass1.getDeclaredField("tenantId");
        tenantIdField.setAccessible(true);
        Object tenantId = tenantIdField.get(object1);
        Class<?> aClass2 = object2.getClass();
        Field channelAccountId1 = aClass2.getDeclaredField("channelAccountId");
        channelAccountId1.setAccessible(true);
        channelAccountId1.set(object2,channelAccountId);
        Field tenantId1 = aClass2.getDeclaredField("tenantId");
        tenantId1.setAccessible(true);
        tenantId1.set(object2,tenantId);
    }
    /**
     * 入一个LocalDateTime对象,获取当天的开始时的localdatetime
     * @param localDateTime
     * @return
     */
    public static LocalDateTime getDayStartTimeByDateTime(LocalDateTime localDateTime){
        return LocalDateTime.parse(localDateTime.format(DATE_TIME_FORMATTER2).substring(0, 11)+"00:00:00",DATE_TIME_FORMATTER2);
    }

    /**
     * 入一个LocalDateTime对象,获取当天的结束时的localdatetime
     * @param localDateTime
     * @return
     */
    public static LocalDateTime getDayEndTimeByDateTime(LocalDateTime localDateTime){
        return LocalDateTime.parse(localDateTime.format(DATE_TIME_FORMATTER2).substring(0, 11)+"23:59:59",DATE_TIME_FORMATTER2);
    }
    /**
     * double乘法,不会四舍五入
     * @param double1
     * @param double2
     * @return
     */
    public static Double doublemultiply(Double double1,Double double2){
        if(double1 == null || double2 == null) throw new NullPointerException();
        Double result = (double1*double2);
        String resultStr = result.toString();
        String[] split = resultStr.split("\\.");
        String splitTwo = split[1];
        splitTwo = splitTwo.length() == 1 ? splitTwo+"0": splitTwo.substring(0,2);
        resultStr = split[0] + "." +splitTwo;
        return Double.parseDouble(resultStr);
    }
    /**
     * double除法,除不尽保留两位小数,不会看第三位的值,不会四舍五入直接丢弃
     * @param double1
     * @param double2
     * @return
     */
    public static Double doubleDivide(Double double1,Double double2){
        if(double1 == null || double2 == null) throw new NullPointerException();
        Double result = (double1/double2);
        String resultStr = result.toString();
        String[] split = resultStr.split("\\.");
        String splitTwo = split[1];
        splitTwo = splitTwo.length() == 1 ? splitTwo+"0": splitTwo.substring(0,2);
        resultStr = split[0] + "." +splitTwo;
        return Double.parseDouble(resultStr);
    }

    /**
     * 在新增数据的时候给数据的基本信息设置值
     * id设置为null,新增和修改时间设置为当前时间,删除标识设置为未删除
     * @param object
     * @throws Exception
     */
    public static void setInsertProperties(Object object) throws Exception {
        Class<?> aClass = object.getClass();
        Field updateTime = aClass.getDeclaredField("updateTime");
        Field createTime = aClass.getDeclaredField("createTime");
        Field delFlag = aClass.getDeclaredField("delFlag");
        Field id = aClass.getDeclaredField("id");
        LocalDateTime now = LocalDateTime.now();
        //1.设置更新时间
        updateTime.setAccessible(true);
        updateTime.set(object,now);
        //2.设置创建时间
        createTime.setAccessible(true);
        createTime.set(object,now);
        //3.设置删除标记
        delFlag.setAccessible(true);
        delFlag.set(object,0);
        //4.设置id
        id.setAccessible(true);
        id.set(object,null);
    }
    /**
     * 获取两个日期之间相差的天数
     * @param start
     * @param end
     * @return
     */
    public static int getDayNum(LocalDate start,LocalDate end){
        return Period.between(start, end).getDays()+1;
    }

    /**
     * 传入年份和月份,获取这个月的第一天的LocalDate对象
     * 例如传入2001和11,就能获取2001-11-01日的对象
     * @param year
     * @param month
     * @return
     */
    public static LocalDate getStartLocalDateByYearAndMonth(Integer year, Integer month){
        if(month != null){
            //获取年月
            return LocalDate.of(year,month,1);
        }else {
            return LocalDate.of(year,1,1);
        }
    }

    /**
     * 这个方法可以返回当日开始时的LocalDateTime
     * 比如今日是1月1号,通过这个方法就可以获取到1月1日00时00分01秒的LocalDateTime对象
     * @return
     */
    public static LocalDateTime getTodayStartDateTime(){
        LocalDate now = LocalDate.now();
        String format = now.format(DATE_TIME_FORMATTER1);
        return LocalDateTime.parse(format+" 00:00:01", DATE_TIME_FORMATTER2);
    }

    /**
     * 这个方法可以返回当日结束时的LocalDateTime
     * 比如今日是1月1号,通过这个方法就可以获取到1月1日23时59分59秒的LocalDateTime对象
     * @return
     */
    public static LocalDateTime getTodayEndDateTime(){
        LocalDate now = LocalDate.now();
        String format = now.format(DATE_TIME_FORMATTER1);
        return LocalDateTime.parse(format+" 23:59:59",DATE_TIME_FORMATTER2);
    }

    /**
     * 传入两个日期类型,获取两个日期之间(包含两个日期)的日期列表
     */
    public static List<LocalDate> getDateList(LocalDate startDate, LocalDate endDate){
        if(startDate == null || endDate == null || startDate.isAfter(endDate)) throw new IllegalArgumentException();
        List<LocalDate> res = new ArrayList<>();
        if(startDate.isEqual(endDate)){
            res.add(startDate);
            return res;
        }
        while (true){
            if(startDate.isEqual(endDate)){
                res.add(startDate);
                break;
            }
            res.add(startDate);
            startDate = startDate.plusDays(1);
        }
        return res;
    }
}
