package com.ljr.jrblog.util;

import com.google.common.collect.Maps;
import io.jsonwebtoken.*;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class TokenUtils {
    private final static long TOKEN_TTL = 1000*60*60*24*7;
    public final static String SIGNATURE = "JR_BLOG_SIGNATURE";
    public static String getToken(Long userId,Long timeStamp){
        JwtBuilder jwtBuilder = Jwts.builder();
        String jwtStr = jwtBuilder
                //设置header信息
                .setHeaderParam("typ","JWT")
                .setHeaderParam("alg","HS256")
                //设置payload信息
                .claim("userId",userId)
                .claim("timeStamp",timeStamp)
                //设置主题
                .setSubject("jrb-user-token")
                //设置生效时长
                .setExpiration(new Date(System.currentTimeMillis()+TOKEN_TTL))
                //设置Id
                .setId(UUID.randomUUID().toString())
                //设置signature信息
                .signWith(SignatureAlgorithm.HS256,SIGNATURE)
                //拼接起来
                .compact();
        return jwtStr;
    }
    public static Map<String,Long> parseToken(String token){
        if(token == null || token.isEmpty()){throw new IllegalArgumentException("token is empty");}
        JwtParser parser = Jwts.parser();
        Jws<Claims> claimsJws;
        claimsJws = parser.setSigningKey(SIGNATURE).parseClaimsJws(token);
        Claims body = claimsJws.getBody();
        Long userId = body.get("userId", Long.class);
        Long timeStamp = body.get("timeStamp", Long.class);
        HashMap<String,Long> map = Maps.newHashMap();
        map.put("userId",userId);
        map.put("timeStamp",timeStamp);
        return map;
    }
}
