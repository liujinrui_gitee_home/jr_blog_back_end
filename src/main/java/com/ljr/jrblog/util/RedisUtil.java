package com.ljr.jrblog.util;

import com.ljr.jrblog.consts.RedisConstant;
import lombok.AllArgsConstructor;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

@Component
@AllArgsConstructor
public class RedisUtil {
    //初始时间戳,2023年12月1日设置为初始时间
    private static final Long INITIAL_TIMESTAMP= 1701388800L;
    private final StringRedisTemplate stringRedisTemplate;

    /**
     * 全局id生成
     * @param keyPrefix 业务前缀
     * @return
     */
    //全局唯一id生成
    public long getNextId(String keyPrefix){
        //1.获取当前时间戳
        LocalDateTime now = LocalDateTime.now();
        long nowTimeStamp = now.toEpochSecond(ZoneOffset.UTC) - INITIAL_TIMESTAMP;
        //2.获取当前日期
        String nowDateStr = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy:MM:dd"));
        //拼接业务前缀
        Long increment = stringRedisTemplate.opsForValue().increment(RedisConstant.ICR_PREFIX +keyPrefix+":"+nowDateStr);
        return nowTimeStamp << 32 | increment;
    }


}
