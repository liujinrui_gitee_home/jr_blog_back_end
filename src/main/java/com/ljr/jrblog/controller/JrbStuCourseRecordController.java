package com.ljr.jrblog.controller;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ljr.jrblog.annos.TokenRequired;
import com.ljr.jrblog.common.R;
import com.ljr.jrblog.context.BaseContext;
import com.ljr.jrblog.pojo.FileStorageEntity;
import com.ljr.jrblog.pojo.JrbStuCourseRecord;
import com.ljr.jrblog.service.JrbStuCourseRecordService;
import com.ljr.jrblog.util.BizUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@AllArgsConstructor
@RestController
@RequestMapping("/stuCourseRecord")
@CrossOrigin
@Api(value = "学生课程记录相关接口")
public class JrbStuCourseRecordController {

    private final JrbStuCourseRecordService jrbStuCourseRecordService;
    private final ObjectMapper objectMapper;
    @TokenRequired
    @PostMapping("/add")
    @ApiOperation("新增")
    public R add(@RequestBody JrbStuCourseRecord record) throws JsonProcessingException {
        if(record == null || !BizUtil.isNotBlankStr(
                record.getName(),record.getIdCardNum(),record.getPhone(),record.getRelativesName(),
                record.getRelativesPhone()) || !BizUtil.isNotNull(record.getStartDate(),record.getEndDate())
        ) return R.fail("参数不全");
        if(record.getStartDate().isAfter(record.getEndDate())) return R.fail("参数错误");
        if(record.getFileUrlsArr() == null){
            record.setFileUrlsArr(new ArrayList<>());
        }else {
            for (FileStorageEntity fileStorageEntity : record.getFileUrlsArr()) {
                if(!BizUtil.isNotBlankStr(fileStorageEntity.getName(),fileStorageEntity.getUrl())) return R.fail("参数不全");
            }
        }
        String fileUrls = objectMapper.writeValueAsString(record.getFileUrlsArr());
        record.setFileUrls(fileUrls);
        record.setUserId(BaseContext.getUserId());
        jrbStuCourseRecordService.save(record);
        return R.ok();
    }

    @TokenRequired
    @ApiOperation("删除")
    @DeleteMapping("/deleteById/{id}")
    public R deleteById(@PathVariable Long id) {
        if(id == null) return R.fail("参数不足");
        JrbStuCourseRecord one = jrbStuCourseRecordService.getById(id);
        if(one == null || one.getDelFlag().equals(1)) return R.ok();
        if(!one.getUserId().equals(BaseContext.getUserId())) return R.fail("无权限删除");
        jrbStuCourseRecordService.removeById(id);
        return R.ok();
    }

    @TokenRequired
    @PutMapping("/update")
    @ApiOperation("更新")
    public R deleteById(@RequestBody JrbStuCourseRecord record) throws JsonProcessingException {
        if(record == null || record.getId() == null) return R.fail("参数不全");
        JrbStuCourseRecord byId = jrbStuCourseRecordService.getById(record.getId());
        if(!byId.getUserId().equals(BaseContext.getUserId())) return R.fail("权限不足");
        if(record.getFileUrlsArr() == null){
            record.setFileUrls(null);
        } else if (record.getFileUrlsArr().isEmpty()) {
            record.setFileUrls("[]");
        } else {
            for (FileStorageEntity fileStorageEntity : record.getFileUrlsArr()) {
                if(!BizUtil.isNotBlankStr(fileStorageEntity.getName(),fileStorageEntity.getUrl())) return R.fail("参数不全");
            }
            record.setFileUrls(objectMapper.writeValueAsString(record.getFileUrlsArr()));
        }
        record.setDelFlag(null);
        record.setCreateTime(null);
        record.setUpdateTime(LocalDateTime.now());
        jrbStuCourseRecordService.updateById(record);
        return R.ok();
    }

    @TokenRequired
    @ApiOperation("通过id查询")
    @GetMapping("/getById/{id}")
    public R getById(@PathVariable Long id) throws JsonProcessingException {
        JrbStuCourseRecord byId = jrbStuCourseRecordService.getById(id);
        if(byId == null || byId.getDelFlag().equals(1)) return R.ok();
        if(!byId.getUserId().equals(BaseContext.getUserId())) return R.fail("权限不足");
        byId.setFileUrlsArr(objectMapper.readValue(byId.getFileUrls(),new TypeReference<List<FileStorageEntity>>(){}));
        return R.ok(byId);
    }

    @TokenRequired
    @DeleteMapping("/deleteBatchIds")
    @ApiOperation("批量删除")
    public R deleteBatchIds(@RequestBody Long[] ids) {
        if(ids == null || ids.length == 0) return R.ok();
        Long userId = BaseContext.getUserId();
        return jrbStuCourseRecordService.deleteBatchIds(userId,ids);
    }

    @TokenRequired
    @GetMapping("/getAllNames")
    @ApiOperation("查出所有的人的名字")
    public R getAllNames() {
        Long userId = BaseContext.getUserId();
        return jrbStuCourseRecordService.getAllNames(userId);
    }

    @TokenRequired
    @GetMapping("/page")
    public R page(Page page,String keyWords,String name,Integer dayNum) throws JsonProcessingException {
        BizUtil.pageParamValid(page);
        Long userId = BaseContext.getUserId();
        if(dayNum != null && dayNum <= 0) return R.fail("非法参数");
        if(dayNum != null){
            LocalDate now = LocalDate.now();
            LocalDate offset = now.plusDays(dayNum);
            List<JrbStuCourseRecord> list = jrbStuCourseRecordService.lambdaQuery()
                    .le(JrbStuCourseRecord::getEndDate, offset)
                    .ge(JrbStuCourseRecord::getEndDate, now).list();
            if(list == null) list = new ArrayList<>();
            if(list.isEmpty()){
                page.setRecords(list);
                page.setTotal(0);
                return R.ok(page);
            }
            Set<String> ids = new HashSet<>();
            for (JrbStuCourseRecord jrbStuCourseRecord : list) {
                LambdaQueryWrapper<JrbStuCourseRecord> wrapper = new LambdaQueryWrapper<>();
                wrapper.eq(JrbStuCourseRecord::getIdCardNum, jrbStuCourseRecord.getIdCardNum())
                        .orderByDesc(JrbStuCourseRecord::getId).last("limit 1");
                JrbStuCourseRecord one = jrbStuCourseRecordService.getOne(wrapper);
                if(one == null){
                    ids.add(jrbStuCourseRecord.getIdCardNum());
                }else {
                    if(one.getId().equals(jrbStuCourseRecord.getId())) ids.add(jrbStuCourseRecord.getIdCardNum());
                }
            }
            if(ids.isEmpty()){
                page.setRecords(new ArrayList<>());
                page.setTotal(0);
                return R.ok(page);
            }
            LambdaQueryWrapper<JrbStuCourseRecord> wrapper = new LambdaQueryWrapper<>();
            wrapper.le(JrbStuCourseRecord::getEndDate, offset)
                    .ge(JrbStuCourseRecord::getEndDate, now)
                    .in(JrbStuCourseRecord::getIdCardNum, ids)
                    .and(BizUtil.isNotBlankStr(keyWords),
                            w -> w.like(JrbStuCourseRecord::getName, keyWords)
                                    .or()
                                    .like(JrbStuCourseRecord::getIdCardNum, keyWords)
                                    .or()
                                    .like(JrbStuCourseRecord::getPhone, keyWords))
                    .and(BizUtil.isNotBlankStr(name), r -> r.eq(JrbStuCourseRecord::getName, name))
                    .orderByDesc(JrbStuCourseRecord::getCreateTime);
            Page pageRes = jrbStuCourseRecordService.page(page,wrapper);
                    List<JrbStuCourseRecord> records = pageRes.getRecords();
            for (JrbStuCourseRecord record : records) {
                record.setFileUrlsArr(objectMapper.readValue(record.getFileUrls(),new TypeReference<List<FileStorageEntity>>(){}));
            }
            pageRes.setRecords(records);
            return R.ok(pageRes);
        }else {
            Page pageRes = jrbStuCourseRecordService.lambdaQuery().eq(JrbStuCourseRecord::getUserId, userId)
                    .and(BizUtil.isNotBlankStr(keyWords),
                            wrapper -> wrapper.like(JrbStuCourseRecord::getName, keyWords)
                                    .or()
                                    .like(JrbStuCourseRecord::getIdCardNum, keyWords)
                                    .or()
                                    .like(JrbStuCourseRecord::getPhone, keyWords))
                    .and(BizUtil.isNotBlankStr(name),wrapper -> wrapper.eq(JrbStuCourseRecord::getName,name))
                    .orderByDesc(JrbStuCourseRecord::getCreateTime)
                    .page(page);
            List<JrbStuCourseRecord> records = pageRes.getRecords();
            for (JrbStuCourseRecord record : records) {
                record.setFileUrlsArr(objectMapper.readValue(record.getFileUrls(),new TypeReference<List<FileStorageEntity>>(){}));
            }
            pageRes.setRecords(records);
            return R.ok(pageRes);
        }
    }
}
