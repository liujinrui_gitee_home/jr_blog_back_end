package com.ljr.jrblog.controller;

import com.ljr.jrblog.service.JrbArticleDataService;
import com.ljr.jrblog.service.JrbArticleService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@AllArgsConstructor
@RestController
@RequestMapping("/articleData")
@CrossOrigin
public class JrbArticleDataController {
    private final JrbArticleDataService jrbArticleDataService;


}
