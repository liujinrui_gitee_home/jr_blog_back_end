package com.ljr.jrblog.controller;

import com.ljr.jrblog.service.MinioFileService;
import com.ljr.jrblog.util.BizUtil;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import com.ljr.jrblog.common.R;

import java.util.Scanner;

@AllArgsConstructor
@RestController
@RequestMapping("/file")
@CrossOrigin
public class FileController {

    private final MinioFileService fileService;

    @PostMapping("/upload")
    public R fileUpload(MultipartFile file) {
        if(file == null || file.isEmpty()) return R.fail("参数不足");
        return R.ok(fileService.fileUpload(file));
    }

    @DeleteMapping("/delete")
    public R fileUpload(String filePath) {
        if(!BizUtil.isNotBlankStr(filePath)) return R.fail("参数不足");
        return fileService.fileDelete(filePath);
    }

}
