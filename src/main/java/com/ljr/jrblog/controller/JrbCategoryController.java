package com.ljr.jrblog.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ljr.jrblog.annos.TokenRequired;
import com.ljr.jrblog.common.R;
import com.ljr.jrblog.context.BaseContext;
import com.ljr.jrblog.pojo.JrbCategory;
import com.ljr.jrblog.service.JrbCategoryService;
import com.ljr.jrblog.util.BizUtil;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;


@AllArgsConstructor
@RestController
@RequestMapping("/category")
@CrossOrigin
public class JrbCategoryController {
    private final JrbCategoryService jrbCategoryService;

    @TokenRequired
    @DeleteMapping("/deleteById/{id}")
    public R add(@PathVariable Long id) {
        if(id == null) return R.fail("参数不足");
        JrbCategory byId = jrbCategoryService.getById(id);
        if(byId == null || byId.getDelFlag().equals(1)) return R.ok();
        if(!byId.getUserId().equals(BaseContext.getUserId())) return R.fail("无权限删除");
        jrbCategoryService.removeById(byId.getId());
        return R.ok();
    }


    @TokenRequired
    @PostMapping("/add")
    public R add(@RequestBody JrbCategory category) throws Exception {
        if(category == null || !BizUtil.isNotBlankStr(category.getName()) ||
                !BizUtil.isNotNull(category.getId())) return R.fail("参数不足");
        return jrbCategoryService.add(category);
    }

    @TokenRequired
    @PutMapping("/update")
    public R update(@RequestBody JrbCategory category) throws Exception {
        if(category == null || !BizUtil.isNotBlankStr(category.getName())) return R.fail("参数不足");
        return jrbCategoryService.bizUpdate(category);
    }

    @TokenRequired
    @GetMapping("/page")
    public R add(Page page, JrbCategory category) throws Exception {
        if(category.getStartTime() != null && category.getEndTime() != null &&
                category.getStartTime().isAfter(category.getEndTime())) return R.fail("参数错误");
        if(page == null) page = new Page(1,10);
        LambdaQueryWrapper<JrbCategory> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(JrbCategory::getUserId, BaseContext.getUserId())
                .like(BizUtil.isNotBlankStr(category.getName()), JrbCategory::getName, category.getName())
                .ge(category.getStartTime() != null, JrbCategory::getCreateTime, category.getStartTime())
                .le(category.getEndTime() != null, JrbCategory::getCreateTime, category.getEndTime())
                .orderByDesc(JrbCategory::getCreateTime);
        return R.ok(jrbCategoryService.page(page,wrapper));
    }

}
