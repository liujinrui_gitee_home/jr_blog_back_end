package com.ljr.jrblog.controller;
import com.ljr.jrblog.annos.TokenRequired;
import com.ljr.jrblog.common.R;
import com.ljr.jrblog.pojo.JrbUserInfo;
import com.ljr.jrblog.service.JrbUserInfoService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
@AllArgsConstructor
@RestController
@RequestMapping("/userInfo")
@CrossOrigin
public class JrbUserInfoController {
    private final JrbUserInfoService userInfoService;

    @PutMapping("/updateByToken")
    @TokenRequired
    public R updateByToken(@RequestBody JrbUserInfo jrbUserInfo){
        if(jrbUserInfo == null) return R.fail("参数不足");
        jrbUserInfo.setDelFlag(0);
        jrbUserInfo.setCreateTime(null);
        jrbUserInfo.setUpdateTime(null);
        return userInfoService.updateByToken(jrbUserInfo);
    }

}
