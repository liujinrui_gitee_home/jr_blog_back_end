package com.ljr.jrblog.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.ljr.jrblog.common.R;
import com.ljr.jrblog.pojo.JrbIvc;
import com.ljr.jrblog.service.JrbIvcService;
import com.ljr.jrblog.util.IVCUtil;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.time.LocalDateTime;

@RestController
@CrossOrigin
@RequestMapping("/ivc")
@AllArgsConstructor
public class JrbIvcController {
    private final JrbIvcService jrbIvcService;
    @GetMapping("/get")
    public void get(HttpServletResponse response) throws IOException {
        IVCUtil ivc = new IVCUtil();
        BufferedImage image = ivc.getImage();
        String value = ivc.getText();
        LocalDateTime now = LocalDateTime.now();
        JrbIvc jrbIvc = new JrbIvc(null,value, now,now);
        jrbIvcService.save(jrbIvc);
        Long id = jrbIvc.getId();
        ServletOutputStream outputStream = response.getOutputStream();
        response.setHeader("Access-Control-Expose-Headers","ivcId");
        response.setHeader("ivcId",id.toString());
        IVCUtil.output(image,outputStream);
        outputStream.close();
    }
}
