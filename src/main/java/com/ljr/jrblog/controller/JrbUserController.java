package com.ljr.jrblog.controller;

import com.ljr.jrblog.annos.TokenRequired;
import com.ljr.jrblog.common.R;
import com.ljr.jrblog.context.BaseContext;
import com.ljr.jrblog.pojo.JrbUser;
import com.ljr.jrblog.pojo.JrbUserInfo;
import com.ljr.jrblog.pojo.UpdatePasswordDto;
import com.ljr.jrblog.service.JrbUserInfoService;
import com.ljr.jrblog.service.JrbUserService;
import com.ljr.jrblog.util.BizUtil;
import com.ljr.jrblog.util.RequestContextUtil;
import com.ljr.jrblog.util.TokenUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jdk.nashorn.internal.parser.Token;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

@AllArgsConstructor
@RestController
@RequestMapping("/user")
@CrossOrigin
@Api(value = "用户相关操作")
public class JrbUserController {
    private final JrbUserService userService;
    private final JrbUserInfoService jrbUserInfoService;

    @PutMapping("/updatePassword")
    @ApiOperation(value = "修改密码")
    @TokenRequired
    public R updatePassword(@RequestBody UpdatePasswordDto dto) throws Exception {
        if(dto == null || !BizUtil.isNotBlankStr(dto.getNewPassword(),
                dto.getOldPassword())) return R.fail("参数不足");
        if(dto.getNewPassword().length() < 5) return R.fail("密码过于简单");
        return userService.updatePassword(dto,BaseContext.getUserId());
    }

    @PostMapping("/register")
    @ApiOperation(value = "用户注册")
    public R register(@RequestBody JrbUser jrbUser) throws Exception {
        if(!BizUtil.isNotNull(jrbUser) || !BizUtil.isNotBlankStr(jrbUser.getUsername()
                ,jrbUser.getPassword(),jrbUser.getIvcCode()) || jrbUser.getIvcId() == null) return R.fail("参数不足");
        if(jrbUser.getUsername().length() > 12) return R.fail("用户名最长为12");
        if(jrbUser.getPassword().length() < 5) return R.fail("密码过于简单");
        return userService.register(jrbUser);
    }
    @PostMapping("/login")
    @ApiOperation(value = "登录")
    public R login(HttpServletRequest request, @RequestBody JrbUser jrbUser) throws InterruptedException {
        if(!BizUtil.isNotNull(jrbUser) || !BizUtil.isNotBlankStr(jrbUser.getUsername()
                ,jrbUser.getPassword(),jrbUser.getIvcCode()) || jrbUser.getTimeStamp() == null
                || jrbUser.getIvcId() == null) return R.fail("参数不足");
        return userService.login(request,jrbUser);
    }

    @GetMapping("/getUserInfoByToken")
    @TokenRequired
    @ApiOperation(value = "通过用户token获取用户信息")
    public R getUserInfoByToken(){
        JrbUser byId = userService.getById(BaseContext.getUserId());
        desensitizeUserInfo(byId);
        JrbUserInfo one = jrbUserInfoService.lambdaQuery().eq(JrbUserInfo::getUserId, byId.getId()).last("limit 1").one();
        byId.setUserInfo(one);
        return R.ok(byId);
    }

    /**
     * 用户数据脱敏
     * @param user
     */
    public static void desensitizeUserInfo(JrbUser user){
        if(user != null){
            user.setPassword(null);
            user.setSalt(null);
        }
    }
}
