package com.ljr.jrblog.pojo;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("jrb_stu_course_record")
public class JrbStuCourseRecord extends Model<JrbStuCourseRecord> {
    @TableId(type = IdType.AUTO)
    private Long id;
    private Long userId;
    private String name;
    private String idCardNum;
    private String phone;
    private String fileUrls;
    @TableField(exist = false)
    private List<FileStorageEntity> fileUrlsArr;
    private String relativesName;
    private String relativesPhone;
    private LocalDate startDate;
    private LocalDate endDate;
    @TableLogic
    private Integer delFlag;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private LocalDateTime createTime;
    private LocalDateTime updateTime;
}
