package com.ljr.jrblog.pojo;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("jrb_user")
public class JrbUser extends Model<JrbUser> {
    @TableId(type = IdType.AUTO)
    private Long id;
    private String username;
    private String password;
    @TableField(exist = false)
    private Long timeStamp;
    @TableField(exist = false)
    private Long ivcId;
    @TableField(exist = false)
    private String ivcCode;
    private String salt;
    @TableLogic
    private Integer delFlag;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private LocalDateTime createTime;
    private LocalDateTime updateTime;
    @TableField(exist = false)
    private JrbUserInfo userInfo;
}
