//package com.ljr.jrblog.interceptor;
//import com.ljr.jrblog.context.BaseContext;
//import com.ljr.jrblog.util.BizUtil;
//import com.ljr.jrblog.util.TokenUtils;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.lang.Nullable;
//import org.springframework.stereotype.Component;
//import org.springframework.web.servlet.HandlerInterceptor;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//@Component
//@Slf4j
//public class LoginTokenInterceptor implements HandlerInterceptor {
//    @Override
//    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
//        String token = request.getHeader("User-Token");
//        if(!BizUtil.isNotBlankStr(token)){
//            log.info("无法获取token,已拦截");
//            return false;
//        }
//        Long userId;
//        try {
//            userId = TokenUtils.parseToken(token);
//        }catch (Exception e){
//            return false;
//        }
//        BaseContext.setUserId(userId);
//        return true;
//    }
//    @Override
//    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, @Nullable Exception ex){
//        BaseContext.remove();
//    }
//}
