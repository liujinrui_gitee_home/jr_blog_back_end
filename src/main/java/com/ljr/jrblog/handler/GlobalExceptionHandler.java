//package com.ljr.jrblog.handler;
//
//import com.ljr.jrblog.common.R;
//import org.springframework.web.bind.annotation.ExceptionHandler;
//import org.springframework.web.bind.annotation.RestControllerAdvice;
//
//@RestControllerAdvice
//public class GlobalExceptionHandler {
//    @ExceptionHandler(Exception.class)
//    public R exceptionHandle(Exception e){
//        return R.fail("服务内部错误");
//    }
//}
