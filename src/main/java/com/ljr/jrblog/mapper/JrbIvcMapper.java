package com.ljr.jrblog.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ljr.jrblog.pojo.JrbIvc;
import com.ljr.jrblog.pojo.JrbUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface JrbIvcMapper extends BaseMapper<JrbIvc> {
}
