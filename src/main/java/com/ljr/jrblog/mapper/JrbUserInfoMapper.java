package com.ljr.jrblog.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ljr.jrblog.pojo.JrbUser;
import com.ljr.jrblog.pojo.JrbUserInfo;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface JrbUserInfoMapper extends BaseMapper<JrbUserInfo> {

}
