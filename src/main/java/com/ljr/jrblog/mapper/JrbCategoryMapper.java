package com.ljr.jrblog.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ljr.jrblog.pojo.JrbCategory;
import com.ljr.jrblog.pojo.JrbUserInfo;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface JrbCategoryMapper extends BaseMapper<JrbCategory> {

}
