package com.ljr.jrblog.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ljr.jrblog.pojo.JrbArticle;
import com.ljr.jrblog.pojo.JrbUser;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface JrbArticleMapper extends BaseMapper<JrbArticle> {

}
