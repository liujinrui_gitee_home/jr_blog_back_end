package com.ljr.jrblog.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ljr.jrblog.pojo.JrbArticleData;
import com.ljr.jrblog.pojo.JrbLike;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface JrbLikeMapper extends BaseMapper<JrbLike> {

}
