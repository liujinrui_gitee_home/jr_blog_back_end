package com.ljr.jrblog.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ljr.jrblog.pojo.JrbLike;
import com.ljr.jrblog.pojo.JrbStuCourseRecord;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Mapper
public interface JrbStuCourseRecordMapper extends BaseMapper<JrbStuCourseRecord> {

    List<String> getAllNames(@Param("userId") Long userId);
}
