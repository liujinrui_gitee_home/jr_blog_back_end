package com.ljr.jrblog.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ljr.jrblog.pojo.JrbUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface JrbUserMapper extends BaseMapper<JrbUser> {

    void updatePasswordById(@Param("id") Long id, @Param("finalPassword") String finalPassword);
}
