package com.ljr.jrblog.common;

import lombok.Data;

/**
 * rest接口返回统一格式
 */
@Data
public class R<T> {
    private static final int SUCCESS_CODE = 0;
    private static final int FAIL_CODE = 1;
    private static final int NO_AUTH_CODE = 401;
    private R(){}
    private int code;
    private String msg;
    private T data;
    public static <T> R<T> ok(T data, String msg) {
        R<T> res = new R<>();
        res.setCode(SUCCESS_CODE);
        res.setData(data);
        res.setMsg(msg);
        return res;
    }
    public static <T> R<T> ok(T data) {
        return ok(data,null);
    }
    public static <T> R<T> ok() {
        return ok(null,null);
    }
    public static <T> R<T> fail(String msg) {
        R<T> res = new R<>();
        res.setCode(FAIL_CODE);
        res.setMsg(msg);
        return res;
    }
    public static <T> R<T> fail() {
        return fail(null);
    }
    public static <T> R<T> noAuth() {
        R<T> res = new R<>();
        res.setCode(NO_AUTH_CODE);
        res.setMsg("未登录或登录信息过期");
        return res;
    }
}
